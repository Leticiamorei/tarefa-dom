let secao = document.querySelector(".section_msg");

function enviar_mensagem(){
    let input = document.querySelector(".input_msg");
    let escopo = document.createElement("div");
    let texto = document.createElement("p");
    texto.innerText = input.value;
    escopo.append(texto);
    let secao = document.querySelector(".section_msg");
    secao.append(escopo);

}

function editar_msg(){
    secao.innerHTML ="";
}

let botaoEnviar = document.querySelector(".botao_enviar");

botaoEnviar.addEventListener("click", () => {enviar_mensagem()});

let botaoExcluir = document.querySelector(".botao_excluir");

botaoExcluir.addEventListener("click", () => {editar_msg()});
